from django.db.models import fields
from rest_framework import serializers
from apps.estacionamento.models import Veiculo, Estacionamento

class VeiculoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Veiculo
        fields = ("__all__")

class EstacionamentoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estacionamento
        fields = ("__all__")
