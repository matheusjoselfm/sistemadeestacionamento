from django.urls import path
from .views import *

urlpatterns = [
    path('veiculos/', VeiculoView.as_view(), name='list-veiculos'),
    path('veiculos/<int:pk>/', VeiculoViewDetalhe.as_view()),
    path('estacionamento/', EstacionamentoView.as_view()),
    path('estacionamento/<int:pk>/', EstacionamentoViewDetalhe.as_view()),
    path('veiculos_estacionados/', VeiculosEstacionados.as_view()),

]