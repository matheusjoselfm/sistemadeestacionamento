from django.test import TestCase, Client
from .models import Veiculo, Estacionamento
from django.urls import reverse

class TestVeiculo(TestCase):
    def setUp(self) -> None:
        self.veiculo = Veiculo.objects.create(
            nome_cliente="Cliente teste",
            modelo_carro="GOLF",
            placa="ABC",
            cor="BRANCO"
        )

    def test_deve_verificar_cadastro_de_carro(self):
        
        self.assertEquals(self.veiculo.nome_cliente, "Cliente teste")
        self.assertEquals(self.veiculo.modelo_carro, "GOLF")
        self.assertEquals(self.veiculo.placa, "ABC")
        self.assertEquals(self.veiculo.cor, "BRANCO")

    def test_deve_buscar_carro_registrado(self):
        
        veiculo = Veiculo.objects.get(id=1)

        self.assertEquals(self.veiculo.id, veiculo.id)
        self.assertEquals(self.veiculo.nome_cliente, veiculo.nome_cliente)

    def test_obtem_carros(self):
        cliente = Client()

        resposta = cliente.get(reverse('list-veiculos'))

        veiculos = resposta.json()
        veiculo = veiculos[0]


        self.assertEquals(veiculo.get("nome_cliente"), "Cliente teste")
        self.assertEquals(veiculo.get("modelo_carro"), "GOLF")
        self.assertEquals(veiculo.get("placa"), "ABC")
        self.assertEquals(veiculo.get("cor"), "BRANCO")

