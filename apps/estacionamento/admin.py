from django.contrib import admin
from .models import Veiculo, Estacionamento

admin.site.register(Veiculo)
admin.site.register(Estacionamento)